package main.com.randommethod.services;

import java.util.Random;

public class RandomMethodServices {

    private static final String SPACE = " ";

    private Random random;

    public RandomMethodServices(Random random) {
        this.random = random;
    }

    private static final int TIMES_OUT = 10;



    public void getRandomNum(){

        System.out.println(random.nextInt());
    }

    public void getTenRandomNum(){

        for (int time = 0; time<TIMES_OUT; time++){
            System.out.print(random.nextInt()+SPACE);
        }
        System.out.println();
    }

    public void getTenRandomNumZeroToTen(){
        for (int time = 0; time< TIMES_OUT; time++){
            System.out.print(random.nextInt(10)+SPACE);
        }
        System.out.println();
    }

    public void getTenRandomNumTwentyToFifty(){
        for (int time = 0; time< TIMES_OUT; time++){
            System.out.print(20+random.nextInt(30)+SPACE);
        }
        System.out.println();
    }
    public void getTenRandomNumMinusTenToTen(){
        for (int time = 0; time< TIMES_OUT; time++){
            System.out.print(random.nextInt(20)-10+SPACE);
        }
        System.out.println();
    }
    public void getRandomQuantityRandomNum(){
        int timesOut = random.nextInt(12)+3;
        for (int time = 0; time<timesOut; time++){
            System.out.print((random.nextInt(45)-10)+SPACE);
        }
        System.out.println();
    }

}
