package main.com.randommethod;

import main.com.randommethod.services.RandomMethodServices;

import java.util.Arrays;
import java.util.Random;


public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        RandomMethodServices randomMethodServices = new RandomMethodServices(random);

                randomMethodServices.getRandomNum();
                randomMethodServices.getTenRandomNum();
                randomMethodServices.getTenRandomNumZeroToTen();
                randomMethodServices.getTenRandomNumTwentyToFifty();
                randomMethodServices.getTenRandomNumMinusTenToTen();
                randomMethodServices.getRandomQuantityRandomNum();
    }

}
